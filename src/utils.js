import { pick, merge, compact } from 'lodash'

const attrsForPick = attrs =>
  compact(
    attrs.map(attr => {
      if (attr.match(/\[(.*)\]/)) {
        return undefined
      }
      return attr
    })
  )

const attrsForArrayPick = attrs =>
  compact(
    attrs.map(attr => {
      if (!attr.match(/\[(.*)\]/)) {
        return undefined
      }
      return attr
    })
  )

const arrayPick = (obj, attrs) => {
  const result = {}
  for (const attr of attrs) {
    const a = attr.match(/[^\[]*/)[0]
    const arr = attr.match(/\[(.*)\]/)
    const attrsToPick = arr[1].split(', ')
    if (!obj[a]) {
      continue
    }
    result[a] = obj[a].map(el => pick(el, attrsToPick))
  }
  return result
}

export const pickDeep = (obj, attrs) =>
  merge(
    pick(obj, attrsForPick(attrs)),
    arrayPick(obj, attrsForArrayPick(attrs))
  )
