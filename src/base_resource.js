import {
  isUndefined,
  isString,
  pick,
  toNumber,
  map,
  intersection,
  capitalize
} from 'lodash'
import Boom from 'boom'
import BaseQueryBuilder from './base_query_builder'
import { pickDeep } from './utils'

export default class BaseResource {
  constructor(req, reply) {
    this.manager = req.db.manager
    this.req = req
    this.reply = reply
  }

  before = []
  beforeIndex = []
  beforeShow = []
  beforeCreate = []
  beforeUpdate = []
  beforeDestroy = []
  beforeValidate = []

  queryBuilder = BaseQueryBuilder

  readAttrs = []

  get currentUser() {
    return this.req.auth.credentials
  }

  get params() {
    return this.req.params
  }

  get body() {
    return this.req.payload
  }

  get query() {
    return this.req.query
  }

  get writeData() {
    return this.body
  }

  get includes() {
    let includes = []
    if (isString(this.query.includes)) {
      includes = this.query.includes.split(',')
    }
    return intersection(includes, this.allowedRelations)
  }

  get queryOpts() {
    return {}
  }

  index() {
    const q = {
      pagination: {
        page: toNumber(this.query.page, 1),
        pageSize: toNumber(this.query.page_size, 10)
      },
      sort: this.query.sort || 'id'
    }

    const qb = new this.queryBuilder(this.query.filters, this.includes)
    const promise = this.manager
      .forge(this.name)
      .query(qb.query(this.queryOpts))

    q.sort.split(',').forEach(e => promise.orderBy(e))

    return promise
      .fetchPage({
        page: q.pagination.page,
        pageSize: q.pagination.pageSize,
        withRelated: qb.withRelated()
      })
      .then(result => {
        return this.reply({
          data: map(result.serialize(), el => pickDeep(el, this.readAttrs)),
          pagination: result.pagination
        })
      })
      .catch(err => this.internalError(err))
  }

  showing(model) {
    return Promise.resolve(model)
  }

  show() {
    return this.manager
      .forge(this.name)
      .where({ id: this.params.id })
      .fetch({
        withRelated: this.includes
      })
      .then(model => {
        if (model === null) {
          return this.reply(Boom.notFound())
        }
        return this.showing(model).then(model => {
          return this.reply({
            data: pickDeep(model.serialize(), this.readAttrs)
          })
        })
      })
      .catch(err => this.internalError(err))
  }

  validation(target) {
    return new Promise(resolve => resolve(true))
  }

  creating() {
    return Promise.resolve()
  }

  create() {
    return this.validation()
      .then(valid => {
        if (!valid) return
        return this.creating().then(() => {
          return this.manager.create(this.name, this.writeData).then(model => {
            return this.reply({
              data: pick(model.serialize(), this.readAttrs)
            }).code(201)
          })
        })
      })
      .catch(err => this.internalError(err))
  }

  updating(model) {
    return Promise.resolve(model)
  }

  update() {
    return this.manager
      .fetch(this.name, { id: this.params.id })
      .then(model => {
        if (model === null) return this.reply(Boom.notFound())
        return this.validation(model).then(valid => {
          if (!valid) {
            return
          }
          return this.updating(model).then(model => {
            return this.manager.save(model, this.writeData).then(savedModel => {
              return this.reply({
                data: pick(savedModel.serialize(), this.readAttrs)
              })
            })
          })
        })
      })
      .catch(err => this.internalError(err))
  }

  async validate() {
    try {
      let model = null
      if (this.writeData.id) {
        model = await this.manager.fetch(this.name, { id: this.writeData.id })
      }
      const valid = await this.validation(model)
      if (!valid) return
      return this.reply({})
    } catch (err) {
      this.internalError(err)
    }
  }

  destroying(model) {
    return Promise.resolve()
  }

  destroy() {
    return this.manager
      .fetch(this.name, { id: this.params.id })
      .then(model => {
        if (model === null) {
          return this.reply(Boom.notFound())
        }
        return this.destroying(model).then(() => {
          return model.destroy().then(deletedModel => {
            return this.reply({
              data: pick(deletedModel.serialize(), this.readAttrs)
            }).code(204)
          })
        })
      })
      .catch(err => this.internalError(err))
  }

  exec(action) {
    const runAction = act =>
      new Promise((resolve, reject) => this[act](resolve, reject))

    let before = Promise.resolve()

    if (this.before.length) {
      before = Promise.all(this.before.map(runAction))
    }

    return before.then(el => {
      const beforeName = `before${capitalize(action)}`
      if (this[beforeName] && this[beforeName].length) {
        return Promise.all(
          this[`before${capitalize(action)}`].map(runAction)
        ).then(() => this[action]())
      }
      return this[action]()
    })
  }

  internalError(err) {
    console.error(err)
    return this.reply(Boom.badImplementation())
  }
}
