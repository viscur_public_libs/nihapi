import fs from 'fs'
import path from 'path'
import { isArray } from 'lodash'

export default (bookshelf, dirname) => {
  const models = {}
  const modelsDir = path.join(dirname, '../app/models')
  const files = fs.readdirSync(modelsDir)
  for (const file of files) {
    if (file.match(/\.js$/)) {
      const name = file.replace('.js', '')
      /* eslint-disable */
      const model = require(path.join(dirname, `../app/models/${name}`)).default
      if (isArray(model)) {
        models[name] = bookshelf.manager.register(
          bookshelf.Model.extend(...model),
          name
        )
        continue
      }
      models[name] = bookshelf.manager.register(
        bookshelf.Model.extend(model),
        name
      )
      /* eslint-enable*/
    }
  }
  return models
}
