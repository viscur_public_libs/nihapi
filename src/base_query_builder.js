export default class BaseQueryBuilder {
  constructor(filters, includes) {
    this.filters = filters || {}
    this.includes = includes
  }

  query(opts) {
    const self = this
    const methods = Object.getOwnPropertyNames(
      Object.getPrototypeOf(this)
    ).filter(method => method.match(/Filter$/))
    return qb => {
      for (const method of methods) {
        self[method](qb, opts)
      }
      return qb
    }
  }

  withRelated() {
    return this.includes
  }
}
