'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.pickDeep = undefined;

var _lodash = require('lodash');

const attrsForPick = attrs => (0, _lodash.compact)(attrs.map(attr => {
  if (attr.match(/\[(.*)\]/)) {
    return undefined;
  }
  return attr;
}));

const attrsForArrayPick = attrs => (0, _lodash.compact)(attrs.map(attr => {
  if (!attr.match(/\[(.*)\]/)) {
    return undefined;
  }
  return attr;
}));

const arrayPick = (obj, attrs) => {
  const result = {};
  for (const attr of attrs) {
    const a = attr.match(/[^\[]*/)[0];
    const arr = attr.match(/\[(.*)\]/);
    const attrsToPick = arr[1].split(', ');
    if (!obj[a]) {
      continue;
    }
    result[a] = obj[a].map(el => (0, _lodash.pick)(el, attrsToPick));
  }
  return result;
};

const pickDeep = exports.pickDeep = (obj, attrs) => (0, _lodash.merge)((0, _lodash.pick)(obj, attrsForPick(attrs)), arrayPick(obj, attrsForArrayPick(attrs)));