'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _lodash = require('lodash');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (bookshelf, dirname) => {
  const models = {};
  const modelsDir = _path2.default.join(dirname, '../app/models');
  const files = _fs2.default.readdirSync(modelsDir);
  for (const file of files) {
    if (file.match(/\.js$/)) {
      const name = file.replace('.js', '');
      /* eslint-disable */
      const model = require(_path2.default.join(dirname, `../app/models/${name}`)).default;
      if ((0, _lodash.isArray)(model)) {
        models[name] = bookshelf.manager.register(bookshelf.Model.extend(...model), name);
        continue;
      }
      models[name] = bookshelf.manager.register(bookshelf.Model.extend(model), name);
      /* eslint-enable*/
    }
  }
  return models;
};