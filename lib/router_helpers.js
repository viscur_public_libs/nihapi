'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.resource = exports.group = undefined;

var _joi = require('joi');

var _joi2 = _interopRequireDefault(_joi);

var _lodash = require('lodash');

var _hapiNext = require('hapi-next');

var _hapiNext2 = _interopRequireDefault(_hapiNext);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const group = exports.group = (routes, opts = {}) => {
  let result = [];

  routes.forEach(el => {
    result = result.concat(el);
  });

  if (opts.prefix) {
    result.forEach(el => {
      el.path = `${opts.prefix}${el.path}`;
    });
  }

  if (opts.options) {
    return result.map(el => {
      el.config = Object.assign(el.config, opts.options);
      return el;
    });
  }

  return result;
}; // TODO
// should be refactored to a class ?? factory??
// remove coderepeating


const instantiateResource = (ResourceClass, route, req, reply) => {
  try {
    const resource = new ResourceClass(req, reply);
    return resource[route]();
  } catch (err) {
    console.log(err);
  }
};

const defaultHandler = (ResourceClass, route, req, reply, opts = {}) => {
  if (!opts.config || !opts.config.plugins || !opts.config.plugins.before) {
    return instantiateResource(ResourceClass, route, req, reply);
  }
  const stack = opts.config.plugins.before;
  const handler = (req, reply) => instantiateResource(ResourceClass, route, req, reply);
  stack.push(handler);
  const series = new _hapiNext2.default(stack);
  return series.execute(req, reply);
};

const index = (name, route, ResourceClass, Validator, opts) => {
  const result = (0, _lodash.merge)({
    method: 'GET',
    path: `/${name}`,
    config: {
      handler(req, reply) {
        return defaultHandler(ResourceClass, route, req, reply, opts[route]);
      },
      validate: {}
    }
  }, opts[route]);
  result.config.validate = (0, _lodash.merge)(result.config.validate, Validator.index);
  return result;
};

const show = (name, route, ResourceClass, Validator, opts) => {
  const result = (0, _lodash.merge)({
    method: 'GET',
    path: `/${name}/{id}`,
    config: {
      handler(req, reply) {
        return defaultHandler(ResourceClass, route, req, reply, opts[route]);
      },
      validate: {
        params: {
          id: _joi2.default.number().required().description('id to show')
        }
      }
    }
  }, opts[route]);
  result.config.validate = (0, _lodash.merge)(result.config.validate, Validator.show);
  return result;
};

const create = (name, route, ResourceClass, Validator, opts) => {
  const result = (0, _lodash.merge)({
    method: 'POST',
    path: `/${name}`,
    config: {
      handler(req, reply) {
        return defaultHandler(ResourceClass, route, req, reply, opts[route]);
      },
      validate: {}
    }
  }, opts[route]);
  result.config.validate = (0, _lodash.merge)(result.config.validate, Validator.create);
  return result;
};

const update = (name, route, ResourceClass, Validator, opts) => {
  const result = (0, _lodash.merge)({
    method: ['PUT', 'PATCH'],
    path: `/${name}/{id}`,
    config: {
      handler(req, reply) {
        return defaultHandler(ResourceClass, route, req, reply, opts[route]);
      },
      validate: {
        params: {
          id: _joi2.default.number().required().description('id to update')
        }
      }
    }
  }, opts[route]);
  result.config.validate = (0, _lodash.merge)(result.config.validate, Validator.update);
  return result;
};

const destroy = (name, route, ResourceClass, Validator, opts) => {
  const result = (0, _lodash.merge)({
    method: 'DELETE',
    path: `/${name}/{id}`,
    config: {
      handler(req, reply) {
        return defaultHandler(ResourceClass, route, req, reply, opts[route]);
      },
      validate: {
        params: {
          id: _joi2.default.number().required().description('id to delete')
        }
      }
    }
  }, opts[route]);
  result.config.validate = (0, _lodash.merge)(result.config.validate, Validator.destroy);
  return result;
};

const validate = (name, route, ResourceClass, Validator, opts) => {
  const result = (0, _lodash.merge)({
    method: 'POST',
    path: `/${name}/validate`,
    config: {
      handler(req, reply) {
        return defaultHandler(ResourceClass, route, req, reply, opts[route]);
      },
      validate: {}
    }
  }, opts[route]);
  result.config.validate = (0, _lodash.merge)(result.config.validate, Validator.validate);
  return result;
};

const createAction = (name, route, ResourceClass, Validator, opts = {}) => {
  if (route === 'index') {
    return index(name, route, ResourceClass, Validator, opts);
  }
  if (route === 'show') {
    return show(name, route, ResourceClass, Validator, opts);
  }
  if (route === 'create') {
    return create(name, route, ResourceClass, Validator, opts);
  }
  if (route === 'update') {
    return update(name, route, ResourceClass, Validator, opts);
  }
  if (route === 'destroy') {
    return destroy(name, route, ResourceClass, Validator, opts);
  }
  if (route === 'validate') {
    return validate(name, route, ResourceClass, Validator, opts);
  }
  return opts[route];
};

const resource = exports.resource = (name, ResourceClass, Validator, opts = {}) => {
  const actions = opts.actions || ['index', 'show', 'create', 'update', 'destroy', 'validate'];
  const routes = [];
  actions.forEach(route => routes.push(createAction(name, route, ResourceClass, Validator, opts)));
  return routes;
};