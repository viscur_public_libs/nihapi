'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _lodash = require('lodash');

var _boom = require('boom');

var _boom2 = _interopRequireDefault(_boom);

var _base_query_builder = require('./base_query_builder');

var _base_query_builder2 = _interopRequireDefault(_base_query_builder);

var _utils = require('./utils');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

class BaseResource {
  constructor(req, reply) {
    this.before = [];
    this.beforeIndex = [];
    this.beforeShow = [];
    this.beforeCreate = [];
    this.beforeUpdate = [];
    this.beforeDestroy = [];
    this.beforeValidate = [];
    this.queryBuilder = _base_query_builder2.default;
    this.readAttrs = [];

    this.manager = req.db.manager;
    this.req = req;
    this.reply = reply;
  }

  get currentUser() {
    return this.req.auth.credentials;
  }

  get params() {
    return this.req.params;
  }

  get body() {
    return this.req.payload;
  }

  get query() {
    return this.req.query;
  }

  get writeData() {
    return this.body;
  }

  get includes() {
    let includes = [];
    if ((0, _lodash.isString)(this.query.includes)) {
      includes = this.query.includes.split(',');
    }
    return (0, _lodash.intersection)(includes, this.allowedRelations);
  }

  get queryOpts() {
    return {};
  }

  index() {
    const q = {
      pagination: {
        page: (0, _lodash.toNumber)(this.query.page, 1),
        pageSize: (0, _lodash.toNumber)(this.query.page_size, 10)
      },
      sort: this.query.sort || 'id'
    };

    const qb = new this.queryBuilder(this.query.filters, this.includes);
    const promise = this.manager.forge(this.name).query(qb.query(this.queryOpts));

    q.sort.split(',').forEach(e => promise.orderBy(e));

    return promise.fetchPage({
      page: q.pagination.page,
      pageSize: q.pagination.pageSize,
      withRelated: qb.withRelated()
    }).then(result => {
      return this.reply({
        data: (0, _lodash.map)(result.serialize(), el => (0, _utils.pickDeep)(el, this.readAttrs)),
        pagination: result.pagination
      });
    }).catch(err => this.internalError(err));
  }

  showing(model) {
    return Promise.resolve(model);
  }

  show() {
    return this.manager.forge(this.name).where({ id: this.params.id }).fetch({
      withRelated: this.includes
    }).then(model => {
      if (model === null) {
        return this.reply(_boom2.default.notFound());
      }
      return this.showing(model).then(model => {
        return this.reply({
          data: (0, _utils.pickDeep)(model.serialize(), this.readAttrs)
        });
      });
    }).catch(err => this.internalError(err));
  }

  validation(target) {
    return new Promise(resolve => resolve(true));
  }

  creating() {
    return Promise.resolve();
  }

  create() {
    return this.validation().then(valid => {
      if (!valid) return;
      return this.creating().then(() => {
        return this.manager.create(this.name, this.writeData).then(model => {
          return this.reply({
            data: (0, _lodash.pick)(model.serialize(), this.readAttrs)
          }).code(201);
        });
      });
    }).catch(err => this.internalError(err));
  }

  updating(model) {
    return Promise.resolve(model);
  }

  update() {
    return this.manager.fetch(this.name, { id: this.params.id }).then(model => {
      if (model === null) return this.reply(_boom2.default.notFound());
      return this.validation(model).then(valid => {
        if (!valid) {
          return;
        }
        return this.updating(model).then(model => {
          return this.manager.save(model, this.writeData).then(savedModel => {
            return this.reply({
              data: (0, _lodash.pick)(savedModel.serialize(), this.readAttrs)
            });
          });
        });
      });
    }).catch(err => this.internalError(err));
  }

  validate() {
    var _this = this;

    return _asyncToGenerator(function* () {
      try {
        let model = null;
        if (_this.writeData.id) {
          model = yield _this.manager.fetch(_this.name, { id: _this.writeData.id });
        }
        const valid = yield _this.validation(model);
        if (!valid) return;
        return _this.reply({});
      } catch (err) {
        _this.internalError(err);
      }
    })();
  }

  destroying(model) {
    return Promise.resolve();
  }

  destroy() {
    return this.manager.fetch(this.name, { id: this.params.id }).then(model => {
      if (model === null) {
        return this.reply(_boom2.default.notFound());
      }
      return this.destroying(model).then(() => {
        return model.destroy().then(deletedModel => {
          return this.reply({
            data: (0, _lodash.pick)(deletedModel.serialize(), this.readAttrs)
          }).code(204);
        });
      });
    }).catch(err => this.internalError(err));
  }

  exec(action) {
    const runAction = act => new Promise((resolve, reject) => this[act](resolve, reject));

    let before = Promise.resolve();

    if (this.before.length) {
      before = Promise.all(this.before.map(runAction));
    }

    return before.then(el => {
      const beforeName = `before${(0, _lodash.capitalize)(action)}`;
      if (this[beforeName] && this[beforeName].length) {
        return Promise.all(this[`before${(0, _lodash.capitalize)(action)}`].map(runAction)).then(() => this[action]());
      }
      return this[action]();
    });
  }

  internalError(err) {
    console.error(err);
    return this.reply(_boom2.default.badImplementation());
  }
}
exports.default = BaseResource;